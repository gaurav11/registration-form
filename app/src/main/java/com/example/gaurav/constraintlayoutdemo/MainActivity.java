package com.example.gaurav.constraintlayoutdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickRegBtn(View view){
        Log.d(TAG, "onClickRegBtn: ");
        Intent intent = new Intent(this,HomeActivity.class);
        startActivity(intent);
    }
}
